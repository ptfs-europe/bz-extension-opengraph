## Credit
This code is adapted directly from the amazing work done by the Mozilla B-team.
To see the full code-base for BMO (which is wicked), have a look at
https://github.com/mozilla-bteam/bmo/tree/master

## Installation
1.  To install, clone this repository into $BZ_HOME/extensions/OpenGraph
2.  Ensure the entire folder is chowned by your web server (e.g. www-data on
      Debian-based systems)
3.  Lastly, make the following changes to the following files:

-> $BZ_HOME/template/en/default/index.html.tmpl
    12	
    13	[% PROCESS global/header.html.tmpl
    14	   title = "$terms.Bugzilla Main Page"
    15	   header = "Main Page" 
    16	   header_addl_info = "version $constants.BUGZILLA_VERSION"
 +  17	   og_image = "extensions/OpenGraph/web/1200px-Buggie.png"
    18	%]
    19	
    20	[% IF release %]
    21	  <div id="new_release">
    22	    [% IF release.data %]

-> $BZ_HOME/template/en/default/bug/show.html.tmpl
    10	[% bug = bugs.0 %]
    11	
    12	[% IF !header_done %]
    13	  [% PROCESS "bug/show-header.html.tmpl" %]
 -  14	  [% PROCESS global/header.html.tmpl %]
 +  14    [% PROCESS global/header.html.html
 +  15	    og_image = "extensions/OpenGraph/web/1200px-Buggie.png"
 +  16	  %]
    17	[% END %]
    18	
    19	[% IF nextbug %]
    20	  <hr>

## Contact
If you have any questions, please email me - my email can be found in
my GitLab profile: https://gitlab.com/PerplexedTheta. Thanks!
